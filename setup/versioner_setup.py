import os
import sys

from cx_Freeze import Executable, setup

SRC_PATH = "src/versioner"

build_exe_options = {
    "packages": [
        "os",
        "sqlite3",
        "dataclasses",
        "time",
        "inspect",
        "datetime",
        "threading",
    ],
    "excludes": ["tkinter"],
}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name="E-SAT",
    version="0.0.1",
    description="A nfe uploader worker",
    options={
        "build_exe": build_exe_options,
    },
    executables=[
        Executable(os.path.join(SRC_PATH, "versioner.py"), base=base),
    ],
)
