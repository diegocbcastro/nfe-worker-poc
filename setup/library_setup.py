import distutils.command.build
import os
import zipfile

from Cython.Build import cythonize
from setuptools import Extension, setup

__NAME__ = "NLU"
__VERSION__ = "0.0.4"
__DESCRIPTION__ = "NFe library uploader"

VERSIONS_DIR = "versions/"
TARGET_ZIP_DIR = "build-worker/lib.linux-x86_64-3.8"
PATH_OUT = os.path.join(VERSIONS_DIR, f"NLU-{__VERSION__}.zip")


class BuildCommand(distutils.command.build.build):
    def initialize_options(self):
        distutils.command.build.build.initialize_options(self)
        self.build_base = "build-worker/"


modules = [
    Extension(
        "NLU.worker",
        sources=[
            "src/NLU/worker/database_saver.py",
            "src/NLU/worker/main.py",
            "src/NLU/worker/worker.py",
            "src/NLU/worker/xml_parser.py",
        ],
    ),
    Extension(
        "NLU.worker.factory",
        sources=[
            "src/NLU/worker/factory/database.py",
            "src/NLU/worker/factory/parser.py",
        ],
    ),
]

setup(
    name=__NAME__,
    version=__VERSION__,
    description=__DESCRIPTION__,
    include_package_data=False,
    install_requires=[
        "xmltodict",
    ],
    packages=[
        "worker",
        "worker.factory",
    ],
    package_dir={
        "": "src/NLU",
        "factory": "src/NLU/worker/factory",
    },
    cmdclass={
        "build": BuildCommand,
    },
    ext_modules=cythonize(modules, language_level="3"),
)

with zipfile.ZipFile(PATH_OUT, "w") as zfile:
    for dirname, subdirs, files in os.walk(TARGET_ZIP_DIR):
        zfile.write(dirname)
        for filename in files:
            zfile.write(os.path.join(dirname, filename))

try:
    os.remove(os.path.join(VERSIONS_DIR, "versions.txt"))
except Exception:
    pass

with open(os.path.join(VERSIONS_DIR, "versions.txt"), "w") as file:
    file.write(__VERSION__)
