import unittest

from NLU.worker.xml_parser import XMLParser


class XMLParserTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.xml_parser = XMLParser("test/samples/nfe.xml")

    def test_get_data(self):
        data = self.xml_parser.get_data()
        root = data["root"]

        self.assertEqual(list(data.keys())[0], "root")
        self.assertEqual(root.get("nomeConsumidor"), "Diego Cardoso")
        self.assertEqual(root.get("cpf"), "673.636.360-87")

    def test_extract_value_from_key(self):
        nome = self.xml_parser.extract_value_from_key("nomeConsumidor")
        cpf = self.xml_parser.extract_value_from_key("cpf")

        self.assertEqual(nome, "Diego Cardoso")
        self.assertEqual(cpf, "673.636.360-87")
