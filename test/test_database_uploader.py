import os
import unittest
from datetime import datetime

from NLU.worker.database_saver import (
    DatabaseSaver,
    DataModel,
    IdNotNoneException,
)


class DataModelTestCase(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_create_model(self):
        created_at = datetime.now()
        test_model = DataModel(
            None, "Diego Cardoso", 67363636087, created_at=created_at
        )

        self.assertEqual(test_model.id, None)
        self.assertEqual(test_model.customer_name, "Diego Cardoso")
        self.assertEqual(test_model.cpf, 67363636087)
        self.assertEqual(test_model.created_at, created_at)

    def test_to_dictionary(self):
        created_at = datetime.now()
        created_iso = created_at.isoformat()
        test_model = DataModel(
            1, "Diego Cardoso", 67363636087, created_at=created_at
        )
        test_model_dict = test_model.to_dictionary()

        self.assertEqual(test_model_dict.get("id"), 1)
        self.assertEqual(test_model_dict.get("customer_name"), "Diego Cardoso")
        self.assertEqual(test_model_dict.get("cpf"), 67363636087)
        self.assertEqual(test_model_dict.get("created_at"), created_iso)


class DatabaseSaverTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.database_name = "test_database.sqlite3"
        self.database_saver = DatabaseSaver(self.database_name)

    def test_save(self):
        model = DataModel(None, "Diego Cardoso", 67363636087)
        saved = self.database_saver.save(model)

        self.assertTrue(saved)

        model = DataModel(1, "Diego Cardoso", 67363636087)
        self.assertRaises(IdNotNoneException, self.database_saver.save, model)

    def test_get(self):
        model = DataModel(None, "Diego Cardoso", 67363636087)
        self.database_saver.save(model)

        data = self.database_saver.get()
        self.assertIsInstance(data, list)

        model = DataModel(None, "Diego Cardoso", 67363636087)
        self.database_saver.save(model)

        data = self.database_saver.get()
        self.assertIsInstance(data, list)

    def test_update(self):
        created_at = datetime.now()
        model = DataModel(
            None, "Diego Cardoso", 67363636087, created_at=created_at
        )
        self.database_saver.save(model)

        model = DataModel(
            1, "Diego Cardoso", 67363636087, created_at=created_at
        )

        self.database_saver.update(model)

    def test_delete(self):
        model = DataModel(None, "Diego Cardoso", 67363636087)
        self.database_saver.save(model)

        deleted = self.database_saver.delete(1)

        self.assertTrue(deleted)

    def tearDown(self) -> None:
        os.remove(self.database_name)
        return super().tearDown()
