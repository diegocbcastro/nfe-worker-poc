import time
from threading import Thread


class VersionWorker(Thread):
    __stop_thread: bool = False

    def __init__(self):
        super()

    def run(self):
        while not self.__stop_thread:
            print("ok")
            time.sleep(1)

    def _stop_thread(self):
        pass

    def get_metrics(self):
        pass
