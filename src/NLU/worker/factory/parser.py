from abc import abstractmethod
from typing import Dict, Union


class Parser:
    @abstractmethod
    def get_data(self) -> Dict:
        ...

    @abstractmethod
    def extract_value_from_key(self, key: str) -> Union[str, int]:
        ...
