import sqlite3
from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional, Tuple, Union

from .factory.database import DatabaseFactory


class IdNotNoneException(Exception):
    ...


@dataclass
class DataModel:
    id: Optional[int]
    customer_name: str
    cpf: int
    sended: bool = False
    created_at: datetime = datetime.now()

    def __post_init__(self):
        if isinstance(self.created_at, str):
            self.created_at = datetime.fromisoformat(self.created_at)

    def to_dictionary(self):
        return {
            "id": self.id,
            "customer_name": self.customer_name,
            "cpf": self.cpf,
            "sended": self.sended,
            "created_at": self.created_at.isoformat(),
        }

    def get_values(self) -> Tuple:
        return (
            self.id,
            self.customer_name,
            self.cpf,
            self.sended,
            self.created_at.isoformat(),
        )


class DatabaseSaver(DatabaseFactory):
    __table_name = "customers"
    __database_name: str

    def __init__(self, database_name: str):
        self.__database_name = database_name

        with sqlite3.Connection(self.__database_name) as connection:
            cursor = connection.cursor()
            try:
                cursor.execute(
                    f"""
                    CREATE TABLE IF NOT EXISTS "{self.__table_name}" (
                        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                        "customer_name"	TEXT,
                        "cpf" INTEGER,
                        "sended" BOOLEAN,
                        "created_at" TEXT
                    );
                    """
                )
            except sqlite3.Error as ex:
                print("Database could not be created. Ex.:", str(ex))
            except Exception as ex:
                print("Database cannot be open. Ex.:", str(ex))

    def save(self, model: DataModel) -> bool:
        status_result = False

        if model.id is not None:
            raise IdNotNoneException("Not need id. Check update function.")

        with sqlite3.Connection(self.__database_name) as connection:
            try:
                cursor = connection.cursor()
                cursor.execute(
                    f"INSERT INTO {self.__table_name} VALUES (?, ?, ?, ?, ?);",
                    model.get_values(),
                )
                connection.commit()
                status_result = True
            except sqlite3.Error as ex:
                print("Data could not be inserted. Ex.:", str(ex))
            except Exception as ex:
                print("Data could not be inserted. Internal error:", str(ex))

        return status_result

    def get(self, id: Optional[int] = None) -> Union[List[DataModel], None]:
        response = []

        with sqlite3.Connection(self.__database_name) as connection:
            try:
                cursor = connection.cursor()
                cursor.execute(f"SELECT * FROM {self.__table_name}")
                if id is not None:
                    response = [DataModel(*cursor.fetchone())]
                else:
                    for data in cursor.fetchall():
                        response.append(DataModel(*data))
            except sqlite3.Error as ex:
                print("Data could not be finded. Ex.:", str(ex))
            except Exception as ex:
                print("Data could not be finded. Internal error:", str(ex))

        return response

    def update(self, model: DataModel) -> bool:
        status_result = False

        with sqlite3.Connection(self.__database_name) as connection:
            try:
                cursor = connection.cursor()
                command = (
                    f"UPDATE {self.__table_name} "
                    "SET customer_name = ?, "
                    "    cpf = ?, "
                    "    sended = ?, "
                    "    created_at = ? "
                    "WHERE id = ?;"
                )
                cursor.execute(
                    command,
                    (
                        model.customer_name,
                        model.cpf,
                        model.sended,
                        model.created_at.isoformat(),
                        model.id,
                    ),
                )
                connection.commit()
                status_result = True
            except sqlite3.Error as ex:
                print("Data could not be updated. Ex.:", str(ex))
            except Exception as ex:
                print("Data could not be updated. Internal error:", str(ex))

        return status_result

    def delete(self, id: int) -> bool:
        status_result = False

        with sqlite3.Connection(self.__database_name) as connection:
            try:
                cursor = connection.cursor()
                command = (
                    f"DELETE from {self.__table_name} " f"where id = {id}"
                )
                cursor.execute(command)
                status_result = True
            except sqlite3.Error as ex:
                print("Data could not be deleted. Ex.:", str(ex))
            except Exception as ex:
                print("Data could not be deleted. Internal error:", str(ex))

        return status_result
