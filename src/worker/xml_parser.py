from typing import Dict, Union

import xmltodict
from factory.parser import Parser


class XMLParser(Parser):
    def __init__(self, xmlpath: str):
        self.__xmlpath = xmlpath

    def get_data(self) -> Dict:
        data = {}

        with open(self.__xmlpath, "r") as file:
            data = xmltodict.parse(file.read())

        return data

    def extract_value_from_key(self, key: str) -> Union[str, int, None]:
        data = self.get_data()

        if key in data["root"].keys():
            return data["root"][key]
        else:
            return None
