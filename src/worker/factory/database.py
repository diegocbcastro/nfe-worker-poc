from abc import abstractclassmethod
from typing import Any, Union


class DatabaseFactory:
    @abstractclassmethod
    def save(self, *args: Any) -> bool:
        ...

    @abstractclassmethod
    def get(self, id: int) -> Union[Any, None]:
        ...

    @abstractclassmethod
    def update(self, *args: Any) -> bool:
        ...

    @abstractclassmethod
    def delete(self, *args: Any) -> bool:
        ...
