import glob
import inspect
import os
import time
from datetime import datetime
from threading import Thread

import requests

from NLU.worker import database_saver, main, xml_parser

LIBRARY_MAIN_PATH = inspect.getfile(main)
LIST_LIBRARY_PATH = LIBRARY_MAIN_PATH.split("/")
LIBRARY_PATH = "/".join(LIST_LIBRARY_PATH[: len(LIST_LIBRARY_PATH) - 1])
CURRENT_DIR = "/".join(LIST_LIBRARY_PATH[: len(LIST_LIBRARY_PATH) - 3])


def logger(message: str):
    print(f"LOG [{datetime.now().isoformat()}]: '{message}'")


class AutoUpdater:
    __version_url: str = (
        "https://gitlab.com/diegocbcastro/nfe-worker-poc/"
        "-/raw/master/versions/versions.txt?inline=false"
    )
    __path_urifile: str = (
        "https://gitlab.com/diegocbcastro/nfe-worker-poc/"
        "-/raw/master/versions/NLU-%s.zip?inline=false"
    )
    __current_version_fpath: str = "./current_version.txt"

    def run(self):
        logger("Procurando novo patch...")

        version = None

        if not os.path.exists(self.__current_version_fpath):
            with open(self.__current_version_fpath, "w") as file:
                file.write("0.0.0")
                version = "0.0.0"

        with open(self.__current_version_fpath, "r") as file:
            version = file.readline()

        # download version file
        response = requests.get(self.__version_url, allow_redirects=True)

        try:
            os.remove("NLU_versions.txt")
        except Exception:
            pass

        with open("NLU_versions.txt", "wb") as file:
            file.write(response.content)

        with open("NLU_versions.txt", "r") as file:
            if (new_version := file.readline()) != version:
                logger("atualizando versão local")
                open(self.__current_version_fpath, "w").close()
                with open(self.__current_version_fpath, "w") as file:
                    file.write(new_version)
                logger("Nova versão encontrada: %s" % new_version)

        # download zip and unzip new library

    def stop_thread(self):
        self._stop_thread = True


class NFeWorker(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        database = database_saver.DatabaseSaver("nfe-db.sqlite3")

        while True:
            logger("Buscando xmls...")

            for filename in glob.iglob("./nfe-samples/*", recursive=True):
                logger("Analisando [%s]" % filename)

                parser = xml_parser.XMLParser(filename)
                root = parser.get_data()["root"]

                model = database_saver.DataModel(
                    id=None,
                    customer_name=root["nomeConsumidor"],
                    cpf=False,
                    sended=root["cpf"],
                )

                database.save(model)

                print(model)

            time.sleep(10)


if __name__ == "__main__":
    auto_updater = AutoUpdater()
    auto_updater.run()

    worker = NFeWorker()
    worker.start()

    while True:
        time.sleep(10)
