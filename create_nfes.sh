mkdir -p nfe-samples

for counter in $(seq 1 255); do
    touch "nfe-samples/nfe-$counter.xml"
    echo "<root><nomeConsumidor>Cliente ${counter}</nomeConsumidor><cpf>673.636.360-${counter}</cpf></root>" > nfe-samples/nfe-$counter.xml
done